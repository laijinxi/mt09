function [X]=solsup(U,b)
    n=sqrt(length(U));
    tol=0.000000001;
    X(n)=b(n)/U(n,n);
    for i=n-1:-1:1
        term=0;
        if(abs(U(i,i))<tol){
            disp("erreur:le pivot egale 0!!");
            exit(1);
        }end
        for j=i+1:1:n
        term=term+U(i,j)*X(j);
        end
        X(i)=(b(i)-term)/U(i,i);
    end
    
endfunction
