function [fval] = myf(x)
fval=exp(x);
fval=fval/((cos(x))^3+(sin(x))^3);
endfunction
