function [x]=mcnorm(A,y)
    [m,n]=size(A);
    AA=zeros(n,n);
    AA=A'*A;
    Ay=A'*y;
    x=AA\Ay;
endfunction
