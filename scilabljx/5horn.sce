function [p,pprim]=horn(a,t,deta)
    n=length(a);
    p=a(n);
    for i = n-1 : -1 :1
        p=a(i)+(deta - t(i))*p;
    end
    
    pprim=0;
    for k = 1 : n-1
            temp=a(n);
            for j = n-1 : -1 :k+1
                temp=a(j) + (deta-t(j))*temp;
            end
            for l = 1 : k
                if (l~=1) then
                temp=temp*(deta-t(l-1));
                end
            end
            pprim= pprim + temp;
    end
endfunction
