function []=courbe(y,t,N,tau)
    
    x=linspace(0,7,N);
    plot(x,interpol(y,t,x));
    d=Diffdiv(y,t);
    [Ptau,pprim]=horn(d,t,tau);
    plot(x,(x-tau)*pprim+Ptau);
    
endfunction
