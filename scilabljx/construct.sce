function [A]=construct(t,tau)
    m=length(tau);
    n=length(t);
    
    for i=1:m
        for j=i:i+1
            if j==i
                A(i,j)=(tau(i)-t(j+1))/(t(j)-t(j+1));
            else
                A(i,j)=(tau(i)-t(j-1))/(t(j)-t(j-1));
            end
        end
    end
    
endfunction
