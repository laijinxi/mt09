function trace(N,T,cc)
    n=length(T);
    t=linspace(T(1),T(n),N)';
    plot(t,calcg(t,T,cc));
endfunction
