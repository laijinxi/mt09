function [f]=Diffdiv(y,t)
    n=length(t)-1
    for i=0:n
        f(i+1)=y(i+1);
    end
    for k=1:n
        for i=n : -1 : k
            f(i+1)=(f(i+1)-f(i))/(t(i+1)-t(i-k+1));
        end
    end
    
endfunction
