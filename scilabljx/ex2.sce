//racP(a,b,c) permet de calculer les racines du polynôme aX^2+bX+c
function [xP,xM] = racP(a,b,c)
    delta=b^2-4*a*c;
    xP = (-b + sqrt(delta))/(2*a);
    xM = (-b - sqrt(delta))/(2*a);
endfunction


