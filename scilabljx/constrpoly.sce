function [A]=constrpoly(tau)
    m=length(tau);
    A=zeros(m,3);
    
    for i=1:m
        for j=1:3
            A(i,j)=tau(i)^(j-1);
        end
    end
endfunction
