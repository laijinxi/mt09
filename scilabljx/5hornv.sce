function [p]=hornv(a,t,deta)
    m=length(deta);
    for i = 1 : m
        p(i)=horn(a,t,deta(i));
    end
endfunction
