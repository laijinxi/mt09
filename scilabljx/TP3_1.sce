function [i]=place(T,t)
    n=length(T);
    
    N=length(t)
    for k =1 :N
    if t(k)<T(1) | t(k)>T(n)
        error("erreur dans fact: t<T1 ou t>Tn");
    end
    imin=1;
    imax=n;
    while (imax-imin)>1
        mil=floor((imax+imin)/2);
        if t(k)>=T(mil)
            imin=mil;
        else
            imax=mil;
        end
    end
    i(k)=imin;
    end
endfunction
