function tracer(a,t0,T,N,f)
    Z=eulerexpl(a,t0,T,N,f);
    x=linspace(0,5,N);
    plot(x,2*exp(x)-x^2-2*x-2,x,Z,'r:');
endfunction
