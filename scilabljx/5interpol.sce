function [p]=interpol(y,t,deta)
    a=Diffdiv(y,t);
    p=hornv(a,t,deta);
endfunction
