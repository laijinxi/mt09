function [Z]=eulerexpl(a,t0,T,N,f)
    Z(:,1)=a;
    h=T/N;
    for i=1:N-1
        Z(:,i+1)=Z(:,i)+h*f(t0+i*h,Z(:,i));
    end
endfunction
